REM Description: Flipper Zero BadUSB payload displaying Hack In Provence Logo
REM author: Segmentation_Fault
DELAY 2000
GUI SPACE
DELAY 100
STRING terminal
DELAY 500
ENTER
DELAY 2000
STRING nano
DELAY 500
ENTER
DELAY 1000
STRING                           *%@#&@%@%/*(@@@@*#@@@@/                              
ENTER
STRING                   &@@@@@@@,@@@@@@@(@@@@@@@.@@@@&*,&@@@                         
ENTER
STRING              @@@@@@(#@#@&#*.(@@@@@,@@@@@@@@@.%&@@@@@&,#@  % %, .%              
ENTER
STRING            @@@/(@@@@*@@@@@@#*(@,@@@@@@@@#&@@//@@@@@@@(#   .% ,  /@@            
ENTER
STRING             /@@@@@@@%,@@@@@@@@@@@@@#.&@&%/../&@@@(       # & * ,   /           
ENTER
STRING                     *    // #@@@@@@@@@@@@@@@# #   (  (.   *  (    #            
ENTER
STRING                     *   .**.#@@@@@@@@@@@@@@@/ #   #  (,   ,  (    %            
ENTER
STRING             /@@@@@@@&*@@@@@@@@@@@@@#.@@@&/.,/%&@@(       ( & / ,   *           
ENTER
STRING            @@@#*@@@@,@@@@@@&,/@.@@@@@@@@(@@@,(@@@@@@@((   .% *  ,@@            
ENTER
STRING             .@@@@@@#/@/@%/**#&@@@@,@@@@@@@@@,%&@@@@@#*%@  ( #...%              
ENTER
STRING                  .@@@@@@@@,@@@@@@@#@@@@@@@,@@@@@**%@@@                         
ENTER
STRING                           *@@(@@%@@( /@@@@.&@@@@#                              
ENTER
ENTER
STRING  _     ____  ____  _  __   _  _        ____  ____  ____  _     _____ _      ____  _____
ENTER
STRING / \ /|/  _ \/   _\/ |/ /  / \/ \  /|  /  __\/  __\/  _ \/ \ |\/  __// \  /|/   _\/  __/
ENTER
STRING | |_||| / \||  /  |   /   | || |\ ||  |  \/||  \/|| / \|| | //|  \  | |\ |||  /  |  \  
ENTER
STRING | | ||| |-|||  \__|   \   | || | \||  |  __/|    /| \_/|| \// |  /_ | | \|||  \__|  /_ 
ENTER
STRING \_/ \|\_/ \|\____/\_|\_\  \_/\_/  \|  \_/   \_/\_\\____/\__/  \____\\_/  \|\____/\____\
ENTER
ENTER
STRING   _   _   _   _   _     _   _   _   _   _   _     _   _     _   _   _     _   _   _   _  
ENTER
STRING  / \ / \ / \ / \ / \   / \ / \ / \ / \ / \ / \   / \ / \   / \ / \ / \   / \ / \ / \ / \ 
ENTER
STRING ( D | e | g | u | n ) ( s | o | u | l | e | t ) ( e | s ) ( m | a | i ) ( f | o | r | t )
ENTER
STRING  \_/ \_/ \_/ \_/ \_/   \_/ \_/ \_/ \_/ \_/ \_/   \_/ \_/   \_/ \_/ \_/   \_/ \_/ \_/ \_/ 
ENTER
STRING                 _   _   _     _   _   _   _   _   _     _   _   _   _   _                              
ENTER
STRING                / \ / \ / \   / \ / \ / \ / \ / \ / \   / \ / \ / \ / \ / \                             
ENTER
STRING               ( q | u | e ) ( t | o | u | t | i | s ) ( e | n | s | e | n )                            
ENTER
STRING                \_/ \_/ \_/   \_/ \_/ \_/ \_/ \_/ \_/   \_/ \_/ \_/ \_/ \_/                             
ENTER                                                                                    